defmodule SocketListener do
  @moduledoc """
  Documentation for SocketListener.
  """

  @doc """
  Hello world.

  ## Examples

      iex> SocketListener.hello
      :world

  """
  def hello do
    :world
  end
end
