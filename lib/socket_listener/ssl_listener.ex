defmodule SocketListener.SSLListener do

  require Logger

  def accept(port) do
    {:ok, socket} = :ssl.listen(port, listen_opts())
    Logger.info "Accepting SSL connections on port #{port}"
    loop_acceptor(socket)
  end

  defp loop_acceptor(socket) do
    {:ok, client} = :ssl.transport_accept(socket)
    :ok = :ssl.ssl_accept(client)
    {:ok, pid} = Task.Supervisor.start_child(SocketListener.TaskSupervisor, fn -> serve(client) end)
    :ok = :ssl.controlling_process(client, pid)
    loop_acceptor(socket)
  end

  defp serve(socket) do
    socket
    |> read_data()

    serve(socket)
  end

  defp read_data(socket) do
    {:ok, data} = :ssl.recv(socket, 0)
    write_data_to_file(data)
    log_data(data)
    data
  end

  defp log_data(data) do
    Logger.info("RECV: [#{data}]")
  end

  defp write_data_to_file(data) do

    date =
      NaiveDateTime.utc_now()
      |> NaiveDateTime.to_iso8601()

    salt =
      :crypto.strong_rand_bytes(2)
      |> Base.encode16

    File.write("/tmp/SSL-#{date}-#{salt}", data, [:binary])
  end

  defp listen_opts do
    Application.get_env(:socket_listener, :ssl_listen_opts)
  end

end
