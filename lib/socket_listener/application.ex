defmodule SocketListener.Application do
  # See https://hexdocs.pm/elixir/Application.html
  # for more information on OTP Applications
  @moduledoc false

  use Application

  def start(_type, _args) do
    # List all child processes to be supervised
    children = [
      # Starts a worker by calling: SocketListener.Worker.start_link(arg)
      # {SocketListener.Worker, arg},
      {Task.Supervisor, name: SocketListener.TaskSupervisor},
      Supervisor.child_spec(
        {Task, fn -> SocketListener.Listener.accept(port()) end},
        restart: :permanent,
        id: :socket_accept_task
      ),
      Supervisor.child_spec(
        {Task, fn -> SocketListener.SSLListener.accept(ssl_port()) end},
        restart: :permanent,
        id: :ssl_socket_accept_task
      )
    ]

    # See https://hexdocs.pm/elixir/Supervisor.html
    # for other strategies and supported options
    opts = [strategy: :one_for_one, name: SocketListener.Supervisor]
    Supervisor.start_link(children, opts)
  end

  defp port, do: Application.get_env(:socket_listener, :port, 4040)

  defp ssl_port, do: Application.get_env(:socket_listener, :ssl_port, 4041)
end
