defmodule SocketListener.Listener do

  require Logger

  def accept(port) do
    {:ok, socket} = :gen_tcp.listen(port, listen_opts())
    Logger.info "Accepting connections on port #{port}"
    loop_acceptor(socket)
  end

  defp loop_acceptor(socket) do
    {:ok, client} = :gen_tcp.accept(socket)
    {:ok, pid} = Task.Supervisor.start_child(SocketListener.TaskSupervisor, fn -> serve(client) end)
    :ok = :gen_tcp.controlling_process(client, pid)
    loop_acceptor(socket)
  end

  defp serve(socket) do
    socket
    |> read_data()

    serve(socket)
  end

  defp read_data(socket) do
    {:ok, data} = :gen_tcp.recv(socket, 0)
    write_data_to_file(data)
    data
  end

  defp log_data(data) do
    Logger.info("RECV: [#{data}]")
  end

  defp write_data_to_file(data) do

    date =
      NaiveDateTime.utc_now()
      |> NaiveDateTime.to_iso8601()

    salt =
      :crypto.strong_rand_bytes(2)
      |> Base.encode16

    File.write("/tmp/#{date}-#{salt}", data, [:binary])
  end

  defp listen_opts do
    Application.get_env(:socket_listener, :listen_opts)
  end

end